drop table if exists entities;

create table entities (
    id serial primary key,
    url varchar(1000),
    type varchar(100),
    data jsonb
);

drop table if exists coordinates;

create table coordinates (
    id serial,
    coord geometry(POINT)
);

drop table if exists names;

create table names (
    id serial,
    name varchar(200)
);
