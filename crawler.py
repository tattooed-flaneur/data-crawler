import requests
from io import BytesIO
from lxml import etree
from bs4 import BeautifulSoup
import json
import os
import psycopg2
import psycopg2.extras
from postgis.psycopg import register
from postgis import Point

def flaneur_urls():
    urls = [
        'https://www.der-flaneur.rocks/category/ausgehen.html',
        'https://www.der-flaneur.rocks/category/ausgehen2.html'
    ]
    for u in urls:
        html = requests.get(u)
        soup = BeautifulSoup(html.content, 'html.parser')
        for h in soup.find_all('h1'):
            link = h.find('a')
            yield 'https://www.der-flaneur.rocks' + link['href']

def extract_json_ld_script(response):
        soup = BeautifulSoup(response.content, 'html.parser')
        for y in soup.find_all("script", attrs={'type':'application/ld+json'}):
            yield json.loads(y.text)


def tattoo_urls():
    html = requests.get('https://tattoo.saarland/job_listing-sitemap.xml')
    tree = etree.fromstring(html.content)

    for x in tree.xpath('//*[local-name() = "loc"]'):
        url = x.text
        if url.endswith('/'):
            yield url


def extract_coordinates(item):
    type = item['@type']

    if type=='Event':
        geo = item.get('location').get('geo')
    elif type=='Place':
        geo = item.get('geo')
    else:
        geo = None

    if geo:
        return geo['longitude'], geo['latitude']


if __name__ == '__main__':
    #   reset database
    os.system('PGPASSWORD=hackathon psql -h 68.183.222.137 -U hackathon -f db/schema.sql hackathon')

    #   connect to database
    con = psycopg2.connect(host='68.183.222.137', user='hackathon', password='hackathon', database='hackathon')
    register(con)
    psycopg2.extensions.register_adapter(dict, psycopg2.extras.Json)
    cur = con.cursor()

    inputs = [flaneur_urls, tattoo_urls]
    urls = [url for i in inputs for url in i()]

    extractors = [extract_json_ld_script]

    for u in urls:
        print(u)
        response = requests.get(u)
        for e in extractors:
            for item in e(response):
                cur.execute("insert into entities (url, type, data) values (%s, %s, %s) returning id", (u, item['@type'], item))
                id = cur.fetchone()[0]

                coord = extract_coordinates(item)
                if coord:
                    cur.execute('insert into coordinates (id, coord) values (%s, ST_Point(%s,%s))',
                        (id, coord[0], coord[1]))

                cur.execute('insert into names (id, name) values (%s, %s)', (id, item['name']))
                    

    con.commit()
