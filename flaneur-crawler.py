import requests
from io import BytesIO
from lxml import etree
from bs4 import BeautifulSoup
import json

urls = [
    'https://www.der-flaneur.rocks/category/ausgehen.html',
    'https://www.der-flaneur.rocks/category/ausgehen2.html'
]


with open('flaneur.jsonl', 'w') as out:
    for u in urls:
        html = requests.get(u)
        soup = BeautifulSoup(html.content, 'html.parser')
        for h in soup.find_all('h1'):
            link = h.find('a')
            page = 'https://www.der-flaneur.rocks' + link['href']
            c = requests.get(page)
            soup = BeautifulSoup(c.content, 'html.parser')
            for y in soup.find_all("script", attrs={'type':'application/ld+json'}):
                print(y.text)
                data = json.loads(y.text)
                json.dump(data, out)
                out.write('\n')
